
import { Controller, Get, Post, UseGuards, Put, Param, Body, Query } from '@nestjs/common';
import { JwtStrategy } from '../auth/strategies/jwt.strategy';
import { UserService } from './user.service';

@UseGuards(JwtStrategy)
@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Get()
    findAll(@Query() query) {
        if (query?.id) {
            return this.userService.findOne(+query?.id);
        }
        return this.userService.findAll();
    }

    @Post()
    async create(@Body() user: any) {
        return await this.userService.create(user);
    }


    @Put(':id')
    update(@Param('id') id: string, @Body() user: any) {
        return this.userService.update(+id, user);
    }
}
